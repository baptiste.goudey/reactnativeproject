# Shifumi
Our application is a simple rock/paper/scissors it allows you to play online with your friend this popular game

### Prerequisites

What things you need to install the software and how to install them

```
npm : sudo apt-get install npm
serve (for deployment of the app) : npm install -g expo-cli
```

### Installing

To install our project for a development environment all you have to do are these commands below in the app root directory
```
npm install
npm start
```
Then you can scan the generated QRCode with your expo application and use our app (Android)
And if you have an Iphone just open your camera and it will propose you to open our app on your expo app

## Deployment

To deploy our app you will have to do the following instruction
```
expo publish
```

If you want to build an APK file to install our application on your phone use this command line
```
expo build:android
```
And if you want to do it for IOS use this command line
```
expo build:ios
```
Our app is already available on the expo store at the following link : https://expo.io/@vico-p/shifumi
You can scan the QRCode with you expo apps and you will be able to use our app
