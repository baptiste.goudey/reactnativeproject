import {
  loginActionSuccess, loginActionError, createAccountSuccess, createAccountError,
} from '../redux/actions/actions';

export const loginFetch = data => (dispatch) => {
  let token = '';
  // console.log(data);
  fetch('http://18.216.176.43:3000/auth/login', {
    method: 'POST',
    body: data,
  }).then((response) => {
    if (response.ok) {
      token = response.headers.get('authorization');
      return response.json();
    }
    throw new Error('Network response was not ok.');
  }).then((json) => {
    dispatch(loginActionSuccess(json.user.id, token, json.user.username,
      json.user.imageUrl, json.user.email));
  })
    .catch((error) => {
      console.log(error);
      dispatch(loginActionError());
    });
};

export const disconnect = token => (dispatch) => {
  fetch('http://18.216.176.43:3000/auth/logout', {
    method: 'GET',
    headers: {
      Authorization: token,
    },
  }).then(() => {
    // console.log('Success');
    dispatch({ type: 'FLUSH_STATE' });
  })
    .catch(() => {
      dispatch({ type: 'FLUSH_STATE' });
    });
};

export const createFetch = data => (dispatch) => {
  fetch('http://18.216.176.43:3000/auth/register', {
    method: 'POST',
    body: data,
  }).then((response) => {
    // console.log(response);
    if (response.ok) {
      return response.json();
    }
    throw new Error('Network response was not ok.');
  }).then((json) => {
    dispatch(createAccountSuccess(json.username));
  })
    .catch(() => {
      dispatch(createAccountError());
    });
};

export const getRoomFetch = (token, callback) => {
  fetch('http://18.216.176.43:3000/rooms', {
    method: 'GET',
    headers: {
      Authorization: token,
    },
  }).then((response) => {
    if (response.ok) {
      return response.json();
    }
    throw new Error(response.status);
  }).then((json) => {
    // console.log(json);
    callback(json);
  }).catch(() => {
  });
};
