import { createStackNavigator } from 'react-navigation';
import Colors from '../constants/Colors';
import LoginScreen from '../screens/LoginScreen';
import MainTabNavigator from './MainTabNavigator';
import RegisterScreen from '../screens/RegisterScreen';
import GameRoom from '../screens/GameRoom';

export default createStackNavigator({
  Login: {
    screen: LoginScreen,
  },
  Home: {
    screen: MainTabNavigator,
  },
  Register: {
    screen: RegisterScreen,
  },
  Game: {
    screen: GameRoom,
  },
},
{
  cardStyle: { backgroundColor: Colors.defaultBgColor },
  headerMode: 'none',
});
