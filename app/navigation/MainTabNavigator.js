import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import propTypes from 'prop-types';
import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import Colors from '../constants/Colors';
import LobbyScreen from '../screens/LobbyScreen';
// import SettingsScreen from '../screens/SettingsScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
}, {
  cardStyle: { backgroundColor: Colors.defaultBgColor },
  headerMode: 'none',
});


function TabbarIconHome({ focused }) {
  return (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? 'ios-contact'
          : 'md-contact'
      }
    />
  );
}

function TabbarIconLobby({ focused }) {
  return (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? 'ios-home'
          : 'md-home'
      }
    />
  );
}


HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
  tabBarIcon: TabbarIconHome,
};

const LobbyStack = createStackNavigator({
  Hall: LobbyScreen,
});

LobbyStack.navigationOptions = {
  tabBarLabel: 'Hall',
  tabBarIcon: TabbarIconLobby,
};

TabbarIconHome.propTypes = {
  focused: propTypes.bool.isRequired,
};

TabbarIconLobby.propTypes = {
  focused: propTypes.bool.isRequired,
};

/* const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
}; */

export default createBottomTabNavigator({
  HomeStack,
  LobbyStack,
  // SettingsStack,
}, {
  tabBarOptions: {
    activeTintColor: '#2c3e50',
  },
});
