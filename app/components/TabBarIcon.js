import React from 'react';
import { Icon } from 'expo';
import propTypes from 'prop-types';

import Colors from '../constants/Colors';

export default class TabBarIcon extends React.PureComponent {
  render() {
    const { name, focused } = this.props;
    return (
      <Icon.Ionicons
        name={name}
        size={26}
        style={{ marginBottom: -3 }}
        color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
      />
    );
  }
}

TabBarIcon.propTypes = {
  name: propTypes.string.isRequired,
  focused: propTypes.bool.isRequired,
};
