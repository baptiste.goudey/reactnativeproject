export default function user(state = {}, action) {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      console.log('dispatch LOGIN_SUCCESS');
      return ({
        id: action.id,
        token: action.token,
        username: action.username,
        picture: action.picture,
        email: action.email,
        isConnect: true,
      });
    case 'LOGIN_ERROR':
      console.log('dispatch LOGIN_ERROR');
      return ({
        errorLogin: true,
      });
    case 'CREATE_SUCCESS':
      console.log('dispatch CREATE_SUCCESS');
      return ({
        username: action.username,
      });
    case 'CREATE_ERROR':
      console.log('dispatch CREATE_ERROR');
      return ({
        errorCreate: true,
      });
    case 'FLUSH_STATE':
      console.log('dispatch FLUSH_STATE');
      return ({});
    case 'JOIN_ROOM':
      console.log('dispatch JOIN_ROOM');
      return (
        Object.assign({}, state, { roomid: action.roomid })
      );
    case 'LEAVE_ROOM':
      console.log('dispatch LEAVE_ROOM');
      return ({
        id: state.id,
        token: state.token,
        username: state.username,
        picture: state.picture,
        email: state.email,
        isConnect: true,
      });
    default:
      return state;
  }
}
