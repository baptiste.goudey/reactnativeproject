import { combineReducers } from 'redux';
import user from './handler';

export default combineReducers({
  user,
});
