export const loginActionSuccess = (id, token, username, picture, email) => ({
  type: 'LOGIN_SUCCESS',
  token,
  username,
  picture,
  email,
  id,
});

export const loginActionError = () => ({
  type: 'LOGIN_ERROR',
});

export const createAccountSuccess = username => ({
  type: 'CREATE_SUCCESS',
  username,
});

export const createAccountError = () => ({
  type: 'CREATE_ERROR',
});

export const joinRoomDispatch = roomid => ({
  type: 'JOIN_ROOM',
  roomid,
});

export const leaveRoomDispatch = () => ({
  type: 'LEAVE_ROOM',
});
