import React from 'react';
import {
  Image,
  ScrollView,
  View,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Permissions, ImagePicker } from 'expo';
import { Button } from 'react-native-elements';
import { Circle } from 'react-native-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { TextField } from 'react-native-material-textfield';
import propTypes from 'prop-types';
import logo from '../assets/images/logo.jpeg';
import { createFetch } from '../api/request';
import CustomCamera from '../components/Camera';

class RegisterScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      picture: '',
      email: '',
      confPassword: '',
      nameError: false,
      passwordError: false,
      confPasswordError: false,
      emailError: false,
      pictureError: false,
      clickedCreate: false,
      display: false,
      loading: false,
    };
  }


  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    const permission = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== 'granted' || permission.status !== 'granted') this.redirectLoginScreen();
    this.redirectIfCreate(this.props);
  }

  componentDidUpdate() {
    this.redirectIfCreate(this.props);
  }

  static getDerivedStateFromProps(nextProps) {
    const { nameConflict, username } = nextProps;
    const ret = {};
    if (nameConflict || (username !== '' && username !== undefined)) {
      ret.loading = false;
    }
    return (ret);
  }

  redirectIfCreate = (props) => {
    const { navigation, username } = props;
    const { navigate } = navigation;
    if (username !== undefined && username !== '') navigate('Login');
  }

  handlePicture = async (photo) => {
    if (photo !== undefined) this.setState({ picture: photo.uri });
    this.setState({ display: false });
  }

  redirectLoginScreen = () => {
    const { navigation, flushState } = this.props;
    const { navigate } = navigation;
    flushState();
    navigate('Login');
  }

  createAccount = () => {
    const {
      login, password, confPassword, picture, email,
    } = this.state;
    const { createRequest, flushState } = this.props;
    let launchRequest = true;
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.setState({
      clickedCreate: true,
      nameError: false,
      emailError: false,
      passwordError: false,
      passwordMismatchError: false,
      pictureError: false,
    });
    if (login === '') {
      launchRequest = false;
      this.setState({ nameError: true });
    }
    if (password === '') {
      launchRequest = false;
      this.setState({ passwordError: true });
    }
    if (email === '') {
      launchRequest = false;
      this.setState({ emailError: true });
    }
    if (confPassword === '') {
      this.setState({ passwordError: true });
      launchRequest = false;
    }
    if (confPassword !== '' && password !== '' && confPassword !== password) {
      launchRequest = false;
      this.setState({ passwordMismatchError: true });
    }
    if (picture === '') {
      this.setState({ pictureError: true });
      launchRequest = false;
    }
    if (password.length < 8 || !re.test(email)) launchRequest = false;
    flushState();
    if (launchRequest) {
      const uriParts = picture.split('.');
      const fileType = uriParts[uriParts.length - 1];
      const data = new FormData();
      data.append('password', password);
      data.append('username', login.trim());
      data.append('imageFile', {
        uri: picture,
        name: `photo.${fileType}`,
        type: `image/${fileType}`,
      });
      data.append('email', email.trim());
      createRequest(data);
      this.setState({ loading: true });
    }
  }

  checkErrorField = () => {
    const {
      passwordMismatchError, passwordError, emailError, nameError, confPasswordError,
      password, email, pictureError, clickedCreate,
    } = this.state;
    const { nameConflict } = this.props;
    let errConfPwdMsg = '';
    let errPwdMsg = '';
    let errEmailMsg = '';
    let errPicMsg = '';
    let errNameMsg = '';
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (confPasswordError) errConfPwdMsg = 'Ce champ doit être rempli';
    else if (passwordMismatchError) errConfPwdMsg = 'Les champs n\'ont pas le même mot de passe';
    if (emailError) errEmailMsg = 'Ce champ doit être rempli';
    else if (!re.test(email) && clickedCreate) errEmailMsg = 'Entrer un email valide';
    if (passwordError) errPwdMsg = 'Ce champ doit être rempli';
    else if (password.length < 8 && clickedCreate) errPwdMsg = 'Le mot de passe est trop court';
    if (pictureError) errPicMsg = 'Prenez une photo avant de vous enregistrer';
    if (nameError) errNameMsg = 'Ce champ doit être rempli';
    else if (nameConflict) errNameMsg = 'Ce nom de compte est déjà pris';
    return {
      errConfPwdMsg, errPwdMsg, errEmailMsg, errPicMsg, errNameMsg,
    };
  }

  pickImage = async () => {
    const image = await ImagePicker.launchImageLibraryAsync(ImagePicker.MediaTypeOptions.Image,
      true);
    if (!image.cancelled) this.setState({ picture: image.uri });
  }

  render() {
    const {
      login, password, confPassword, display, picture, email, loading,
    } = this.state;
    const {
      errPwdMsg, errPicMsg, errEmailMsg, errNameMsg, errConfPwdMsg,
    } = this.checkErrorField();
    const { height } = Dimensions.get('window');
    if (loading) {
      return (
        <Circle
          style={{ flex: 1, alignSelf: 'center', marginTop: height / 2 - 50 }}
          size={100}
          indeterminate
          color="#2c3e50"
        />
      );
    }
    if (display) {
      return (
        <CustomCamera display={display} pictureHandler={this.handlePicture} />
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <ScrollView contentContainerStyle={{ justifyContent: 'center', alignItems: 'center', paddingBottom: 20 }}>
          <Image source={logo} style={{ marginTop: 170 }} />
          <View style={{ width: 200 }}>
            <TextField
              label="Nom de compte"
              value={login}
              textColor="#2c3e50"
              tintColor="#2c3e50"
              error={errNameMsg}
              onChangeText={loginTmp => this.setState({ login: loginTmp })}
            />
            <TextField
              label="Email"
              value={email}
              textColor="#2c3e50"
              tintColor="#2c3e50"
              error={errEmailMsg}
              onChangeText={emailTmp => this.setState({ email: emailTmp })}
            />
            <TextField
              label="Mot de passe"
              value={password}
              textColor="#2c3e50"
              tintColor="#2c3e50"
              secureTextEntry
              error={errPwdMsg}
              title="Minimum 8 caractère"
              onChangeText={passwordTmp => this.setState({ password: passwordTmp })}
            />
            <TextField
              label="Confirmer mot de passe"
              value={confPassword}
              textColor="#2c3e50"
              tintColor="#2c3e50"
              secureTextEntry
              error={errConfPwdMsg}
              onChangeText={passwordTmp => this.setState({ confPassword: passwordTmp })}
            />
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
              <TextField
                containerStyle={{ flex: 1, alignSelf: 'flex-end' }}
                label="Photo"
                value={picture}
                error={errPicMsg}
                textColor="#2c3e50"
                tintColor="#2c3e50"
                editable={false}
              />
              <Icon
                style={{
                  flex: 0.2, alignSelf: 'flex-end', marginBottom: 15, marginRight: 5, marginLeft: 10,
                }}
                name="camera"
                size={20}
                type="fontawesome"
                color="#2c3e50"
                onPress={() => { this.setState({ display: true }); }}
              />
              <Icon
                style={{
                  flex: 0.2, alignSelf: 'flex-end', marginBottom: 15, marginRight: 5, marginLeft: 10,
                }}
                name="upload"
                size={20}
                type="fontawesome"
                color="#2c3e50"
                onPress={this.pickImage}
              />
            </View>
            <Button
              buttonStyle={{ backgroundColor: '#2c3e50', marginTop: 20 }}
              title="Créer un compte"
              onPress={this.createAccount}
            />
            <Button
              buttonStyle={{ backgroundColor: '#2c3e50', marginTop: 20 }}
              title="Se connecter"
              onPress={this.redirectLoginScreen}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  if (state.user && state.user.errorCreate) return ({ nameConflict: state.user.errorCreate });
  if (state.user) return ({ username: state.user.username });
  return ({});
};

const mapDispatchToProps = dispatch => ({
  createRequest: data => dispatch(createFetch(data)),
  flushState: () => dispatch({ type: 'FLUSH_STATE' }),
});


RegisterScreen.propTypes = {
  createRequest: propTypes.func.isRequired,
  flushState: propTypes.func.isRequired,
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired,
  }).isRequired,
  nameConflict: propTypes.bool,
};

RegisterScreen.defaultProps = {
  nameConflict: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(RegisterScreen);
