import React from 'react';
import {
  Text,
  View,
  BackHandler,
  TouchableOpacity,
  Image,
} from 'react-native';
import TimerCountdown from 'react-native-timer-countdown';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import Dialog from 'react-native-dialog';
import AnimatedEllipsis from 'react-native-animated-ellipsis';
import rock from '../assets/images/rock.jpeg';
import scissors from '../assets/images/scissors.jpeg';
import paper from '../assets/images/paper.png';
import { leaveRoomDispatch } from '../redux/actions/actions';

class GameRoom extends React.Component {
  constructor(props) {
    super(props);
    const { navigation } = this.props;
    const socket = navigation.getParam('socket', undefined);
    this.state = {
      socket,
      waiting: true,
      timer: 0,
      played: false,
      loadingData: false,
      score: 0,
      scoreAdv: 0,
      round: 1,
    };
  }

  componentDidMount() {
    const { leaveRoom, navigation, idplayer } = this.props;
    const {
      socket, score, scoreAdv, round,
    } = this.state;
    BackHandler.addEventListener('hardwareBackPress', () => {
      socket.emit('exitRoom', {}, () => {
        leaveRoom();
      });
      navigation.goBack();
      return true;
    });
    if (socket) {
      socket.on('roundStart', (data) => {
        const { duration } = data;
        this.setState({ waiting: false, timer: duration, loadingData: false });
      });
      socket.on('roundEnd', (data) => {
        const { winner } = data;
        if (winner) {
          const { userId } = winner;
          if (userId === idplayer) {
            let tmpScore = score;
            tmpScore += 1;
            this.setState({ score: tmpScore });
          } else {
            let tmpScoreAdv = scoreAdv;
            tmpScoreAdv += 1;
            this.setState({ scoreAdv: tmpScoreAdv });
          }
          let tmpRound = round;
          tmpRound += 1;
          this.setState({
            round: tmpRound, waiting: false, loadingData: true,
          });
        } else {
          let tmpRound = round;
          tmpRound += 1;
          this.setState({ round: tmpRound, waiting: false, loadingData: false });
        }
      });
      socket.on('gameEnd', (data) => {
        console.log(data);
        this.setState({ gameEnd: true, loadingData: false });
        socket.emit('exitRoom', {}, () => {
          leaveRoom();
        });
      });
    }
  }

  playSign = (sign) => {
    const { socket } = this.state;
    if (socket) {
      socket.emit('playSign', {
        sign,
      }, () => {});
      this.setState({ played: true, waiting: true, loadingData: true });
    }
  }

  goToLobbyScreen = () => {
    const { navigation } = this.props;
    navigation.goBack();
  }

  render() {
    const {
      waiting, gameEnd, timer, played, score, scoreAdv, round, loadingData,
    } = this.state;
    if (waiting) {
      return (
        <View style={{ flex: 1 }}>
          <Text style={{ flex: 1, alignSelf: 'center', marginTop: 300 }}>
            {played ? 'En attente du choix du joueur adverse' : 'En attente de joueur'}
            <AnimatedEllipsis />
          </Text>
        </View>
      );
    }
    if (gameEnd) {
      let result;
      if (score === scoreAdv) result = 'Egalité dommage';
      else if (score > scoreAdv) result = 'Vous avez gagné le match';
      else result = 'Vous avez perdu le match';
      console.log('Final score = ', score, ':', scoreAdv);
      return (
        <View>
          <Dialog.Container visible>
            <Dialog.Title>Fin de partie</Dialog.Title>
            <Dialog.Description>
              {result}
            </Dialog.Description>
            <Dialog.Button label="Ok" onPress={this.goToLobbyScreen} />
          </Dialog.Container>
        </View>
      );
    }
    if (loadingData) {
      return (
        <View style={{ flex: 1 }}>
          <Text style={{ flex: 1, alignSelf: 'center', marginTop: 300 }}>
            Chargement des données
            <AnimatedEllipsis />
          </Text>
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        <TimerCountdown
          initialSecondsRemaining={timer * 1000}
          allowFontScaling
          style={{
            fontSize: 20, alignSelf: 'center', marginTop: 100,
          }}
        />
        <Text style={{ alignSelf: 'center' }}>
      Round
          {' '}
          {round}
        </Text>
        <Text style={{ alignSelf: 'center' }}>
          {score}
          {' '}
      :
          {scoreAdv}
        </Text>
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
          <TouchableOpacity style={{ flex: 1, alignSelf: 'center' }} onPress={() => { this.playSign('rock'); }}>
            <Image source={rock} resizeMode="center" />
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, alignSelf: 'center' }} onPress={() => { this.playSign('paper'); }}>
            <Image source={paper} resizeMode="center" />
          </TouchableOpacity>
          <TouchableOpacity style={{ flex: 1, alignSelf: 'center' }} onPress={() => { this.playSign('scissors'); }}>
            <Image source={scissors} resizeMode="center" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

GameRoom.propTypes = {
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired,
  }).isRequired,
  leaveRoom: propTypes.func.isRequired,
  idplayer: propTypes.number,
};

GameRoom.defaultProps = {
  idplayer: -1,
};

const mapDispatchToProps = dispatch => ({
  leaveRoom: () => dispatch(leaveRoomDispatch()),
});

const mapStateToProps = state => ({
  idplayer: state.user.id,
});

export default connect(mapStateToProps, mapDispatchToProps)(GameRoom);
