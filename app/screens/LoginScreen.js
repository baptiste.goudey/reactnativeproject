import React from 'react';
import {
  Image,
  View,
  Dimensions,
} from 'react-native';
import { Circle } from 'react-native-progress';
import { connect } from 'react-redux';
import { Button } from 'react-native-elements';
import { TextField } from 'react-native-material-textfield';
import propTypes from 'prop-types';
import logo from '../assets/images/logo.jpeg';
import { loginFetch } from '../api/request';

class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      login: '',
      password: '',
      nameError: false,
      passwordError: false,
      loading: false,
    };
  }

  componentDidMount() {
    const { username } = this.props;
    if (username !== '') this.setState({ login: username });
    this.redirectHomeScreen(this.props);
  }

  componentDidUpdate() {
    this.redirectHomeScreen(this.props);
  }

  static getDerivedStateFromProps(nextProps) {
    const { username, errorLogin, token } = nextProps;
    const ret = {};
    if (username !== '') ret.login = username;
    if (errorLogin || (token !== '' && token !== undefined)) {
      ret.loading = false;
    }
    return (ret);
  }

  login = () => {
    const { login, password } = this.state;
    const { loginRequest, flushState } = this.props;
    this.setState({ nameError: false, passwordError: false });
    flushState();
    if (login === '') this.setState({ nameError: true });
    if (password === '') this.setState({ passwordError: true });
    if (login !== '' && password !== '') {
      const data = new FormData();
      data.append('password', password);
      data.append('username', login.trim());
      loginRequest(data);
      this.setState({ loading: true });
    }
  }

  redirectHomeScreen = (props) => {
    const { navigation } = props;
    const { navigate } = navigation;
    const { token, isConnect } = props;
    if (token !== '' && token !== undefined && isConnect) {
      navigate('Home');
    }
  }

  redirectRegisterScreen = () => {
    const { navigation, flushState } = this.props;
    const { navigate } = navigation;
    flushState();
    navigate('Register');
  }

  render() {
    const {
      nameError, passwordError, login, password, loading,
    } = this.state;
    const { errorLogin } = this.props;
    const { height } = Dimensions.get('window');
    let errMsgLogin;
    if (nameError) errMsgLogin = 'Ce champ doit être rempli';
    else if (errorLogin) {
      errMsgLogin = 'Nom de compte ou mot de passe incorrect';
    } else errMsgLogin = '';
    if (loading) {
      return (
        <Circle
          style={{ flex: 1, alignSelf: 'center', marginTop: height / 2 - 50 }}
          size={100}
          indeterminate
          color="#2c3e50"
        />
      );
    }
    return (
      <View style={{ flex: 1, alignItems: 'center' }}>
        <Image source={logo} style={{ marginTop: 170 }} />
        <View style={{ width: 200 }}>
          <TextField
            label="Nom de compte"
            value={login}
            textColor="#2c3e50"
            tintColor="#2c3e50"
            error={errMsgLogin}
            onChangeText={loginTmp => this.setState({ login: loginTmp })}
          />
          <TextField
            label="Mot de passe"
            value={password}
            textColor="#2c3e50"
            tintColor="#2c3e50"
            error={passwordError ? 'Ce champ doit être rempli' : ''}
            secureTextEntry
            onChangeText={passwordTmp => this.setState({ password: passwordTmp })}
          />
          <Button
            buttonStyle={{ backgroundColor: '#2c3e50', marginTop: 20 }}
            title="Connexion"
            onPress={this.login}
          />
          <Button
            buttonStyle={{ backgroundColor: '#2c3e50', marginTop: 20 }}
            title="Créer un compte"
            onPress={this.redirectRegisterScreen}
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  if (state.user && state.user.errorLogin) {
    return ({
      errorLogin: state.user.errorLogin,
    });
  }
  if (state.user) {
    return ({
      isConnect: true,
      token: state.user.token,
      username: state.user.username,
      email: state.user.email,
    });
  }
  return ({
    isConnect: false,
  });
};

const mapDispatchToProps = dispatch => ({
  loginRequest: data => dispatch(loginFetch(data)),
  flushState: () => dispatch({ type: 'FLUSH_STATE' }),
});

LoginScreen.propTypes = {
  loginRequest: propTypes.func.isRequired,
  flushState: propTypes.func.isRequired,
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired,
  }).isRequired,
  username: propTypes.string,
  errorLogin: propTypes.bool,
};

LoginScreen.defaultProps = {
  username: '',
  errorLogin: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
