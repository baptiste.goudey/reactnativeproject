import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  BackHandler,
} from 'react-native';
import { connect } from 'react-redux';
import { material } from 'react-native-typography';
import { Avatar, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import propTypes from 'prop-types';
// import { NavigationEvents } from "react-navigation";
import { disconnect } from '../api/request';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    paddingTop: 30,
  },
});

class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', () => {
      BackHandler.exitApp();
      return true;
    });
    this.redirectLoginScreen(this.props);
  }

  componentDidUpdate() {
    this.redirectLoginScreen(this.props);
  }

  redirectLoginScreen = (props) => {
    const { navigation, logout } = props;
    const { navigate } = navigation;
    const { token, isConnect } = props;
    if (token === '' || token === undefined) {
      navigate('Login');
    }
    if (isConnect === false) {
      if (token !== '' && token !== undefined) logout(token);
      navigate('Login');
    }
  }

  render() {
    const {
      picture, username, logout, email,
    } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <Avatar
            size="xlarge"
            rounded
            source={{ uri: picture.length > 0 ? picture : 'none' }}
            activeOpacity={0.7}
            containerStyle={{ flex: 1, alignSelf: 'center', marginTop: 170 }}
          />
          <View style={{ flex: 1, alignSelf: 'center', marginTop: 50 }}>
            <Text numberOfLines={1} ellipsizeMode="tail" style={Object.assign({}, material.display1, { alignSelf: 'center' })}>
              {username}
            </Text>
            <Text numberOfLines={1} ellipsizeMode="tail" style={Object.assign({}, material.display1, { alignSelf: 'center', marginTop: 20 })}>
              {email}
            </Text>
            <Button
              icon={(
                <Icon
                  name="power-off"
                  type="fontawesome"
                  size={20}
                  color="white"
                />
              )}
              buttonStyle={{
                backgroundColor: '#2c3e50',
                width: 40,
                height: 40,
                borderRadius: 100,
                alignSelf: 'center',
                marginTop: 50,
              }}
              title=""
              onPress={logout}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

HomeScreen.propTypes = {
  navigation: propTypes.shape({
    navigate: propTypes.func.isRequired,
  }).isRequired,
  username: propTypes.string,
  picture: propTypes.string,
  logout: propTypes.func.isRequired,
  email: propTypes.string,
};

HomeScreen.defaultProps = {
  username: '',
  picture: '',
  email: '',
};

const mapStateToProps = (state) => {
  if (state.user) {
    return ({
      isConnect: true,
      token: state.user.token,
      username: state.user.username,
      picture: state.user.picture,
      email: state.user.email,
    });
  }
  return ({
    isConnect: false,
  });
};

const mapDispatchToProps = dispatch => ({
  logout: token => dispatch(disconnect(token)),
});


export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
