const Sequelize = require('sequelize');
const imgur = require('imgur');
const sharp = require('sharp');
const fs = require('fs');
const models = require('../models/index');

exports.users = {
  all() {
    return models.user.findAll({ raw: true });
  },
  one(userId) {
    return models.user.findById(userId, { raw: true });
  },
};

async function uploadImage(path) {
  await sharp(path)
    .jpeg({
      quality: 95,
      chromaSubsampling: '4:4:4',
    })
    .toFile(`${path}resize`);
  const result = await imgur.uploadFile(`${path}resize`);
  fs.unlink(`${path}resize`, (err) => {
    if (err) console.error('Could not delete uploaded resized file', `${path}resize`);
  });
  return result.data.link;
}

exports.auth = {
  checkIfExist(username) {
    return models.user.find({ where: { username }, raw: true });
  },
  login(username, password) {
    return models.user.find({
      where: {
        [Sequelize.Op.and]: [{ username }, { password }],
      },
      raw: true,
    });
  },
  async register(payload) {
    const newPayload = Object.assign({}, payload);
    newPayload.imageUrl = await uploadImage(payload.imageFile.path);
    delete newPayload.imageFile;
    return models.user.create(newPayload, { fields: Object.keys(newPayload) });
  },
};
